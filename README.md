# Breast Cancer Diagnosis
## Data
Dataset contains results of breast image analysis taken on women with breast cancer:
1. radius (mean of distances from center to points on the perimeter)
2. texture (standard deviation of gray-scale values)
3. perimeter
4. area
5. smoothness (local variation in radius lengths)
6. compactness (perimeter^2 / area - 1.0)
7. concavity (severity of concave portions of the contour)
8. concave points (number of concave portions of the contour)
9. symmetry
10. fractal dimension ("coastline approximation" - 1)

For each feature mean, standard error and 'worst' value was provided.
For example 'worst' for concavity_worst means largest mean value for severity of concave portions of the contour.

Dataset has also diagnosis column which contains diagnosis of breast tissues:
- M = malignant (212 rows),
- B = benign (357 rows).


Data came from [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Diagnostic%29).

## Preprocesing
#### Handling nulls
There are no null values what is proven by below table.

|Column    |  #nulls |
|---|---|
| radius_mean             | 0  |
| texture_mean            | 0  |
| perimeter_mean          | 0  |
| area_mean               | 0  |
| smoothness_mean         | 0  |
| compactness_mean        | 0  |
| concavity_mean          | 0  |
| concave points_mean     | 0  |
| symmetry_mean           | 0  |
| fractal_dimension_mean  | 0  |
| radius_se               | 0  |
| texture_se              | 0  |
| perimeter_se            | 0  |
| area_se                 | 0  |
| smoothness_se           | 0  |
| compactness_se          | 0  |
| concavity_se            | 0  |
| concave points_se       | 0  |
| symmetry_se             | 0  |
| fractal_dimension_se    | 0  |
| radius_worst            | 0  |
| texture_worst           | 0  |
| perimeter_worst         | 0  |
| area_worst              | 0  |
| smoothness_worst        | 0  |
| compactness_worst       | 0  |

#### Normalization
There is no reason to normalize data before loading into database. My idea is to build website which will return information about
cancer diagnosis based on provided data. So data should be normalised after adding data provided by user.

#### Categorical values
Dataset has just one categorical column - diagnosis. Based on this column I build new one:
- malignant_cancer (type: boolean) - 1 where diagnosis was equal to M and 0 where diagnosis was equal to B.

#### Multicollinearity
Based on correlation values I decided to drop a few features:
- area_mean (highly correlated with radius_mean),
- perimeter_mean (highly correlated with radius_mean),
- concave points_mean (highly correlated with concavity_mean).

![Mean features correlation](./primary_analysis/plots/mean_analysis.png)

Two first relations are obvious because of math principals. Third relation is also simple to explain, this two features
measure almost the same thing.

![Standard error features correlation](./primary_analysis/plots/se_analysis.png)

![Worst features correlation](./primary_analysis/plots/worst_analysis.png)

Correlation heatmap is similar also for standard error and 'worst' features what is understandable.

## Model fitting
### Clustering algorithms
#### Normalization
Data were normalised using MinMaxScaler from sklearn library.

#### Dimension reduce
Using [PCA analysis](https://code.likeagirl.io/principal-component-analysis-dimensionality-reduction-technique-step-by-step-approach-ffd46623ff67) I created plot of explained variance. Based on below graph I can figure out that 2 dimensions of transformed data explain about 80% of data.

![PCA results](./model_fitting/plots/pca_results.png)

Thereupon I decided to transform data to 2 dimensions.

#### K-means
It's simple method which is based on calculating distances between data points. [Here](https://www.datascience.com/blog/k-means-clustering) you can find more
details about this method. Using this model I will try to find out if values are gathering around common point
or points. It's the most basic one approach to predict diagnosis.

##### Results
Using KMeans() I fitted model to train dataset (70% of whole dataset) and predict
labels for test dataset (30% of whole dataset). On below you can find true values
for test data.

![Test data graph - true labels](./model_fitting/plots/k_means_true.png)

For comparison check below graph with predicted labels.

![Test data graph - predicted labels](./model_fitting/plots/k_means_predicted.png)

Finally to verify results I used confusion matrix, how you can see model is more accurate
for malignant cancer. It is possible that this model is overfitted.

![Confusion matrix - k_means](./model_fitting/plots/k_means_confusion_matrix.png)

Conclusion is that based on dimension reduce and k-means algorithm we are able to
recognize almost 100% cases of malignant cancer.
#### EM clustering
This method is a little bit more complicated than K-means. The main assumption is that data (values) came from the
same distribution with different parameters. Find more [here](http://rstudio-pubs-static.s3.amazonaws.com/154174_78c021bc71ab42f8add0b2966938a3b8.html).

##### Results
Using GaussianMixture() I fitted model to train dataset (70% of whole dataset) and
predict labels for test dataset (30% of whole dataset). On below graph you can find
true labels for test data.

![Test data graph - true labels](./model_fitting/plots/em_true.png)

Here you have predicted labels.

![Test data graph - predicted labels](./model_fitting/plots/em_predicted.png)

How you can seemingly see borders between two groups are more flexible and that is
a reason of confusion matrix looks.

![Confusion matrix - k_means](./model_fitting/plots/em_confusion_matrix.png)

Accuracy on both groups is similar so this model is more stable. It's allow me to predict
malignant and benign with close accuracy.

### Decision tree based algorithms

#### Normalization
There is no reason to normalize data because decision tree based method always standardise
provided data.

#### Dimension reduce
There is no reason to reduce number of dimensions because decision tree should have more
feauters than less to analyze. Thanks to this models can be more accurate.

#### Decision trees
The most simple models from this family, can have high accuracy if patterns are easy to find. Read more [here](https://towardsdatascience.com/everything-you-need-to-know-about-decision-trees-8fcd68ecaa71).

##### Results
Using GridSearchCV I found best parameters for decision tree model. Primary I provided following
possibilities.
```python
params = {
    'criterion': ['entropy', 'gini'],
    'splitter': ['best', 'random'],
    'max_features': ['sqrt', 'log2', None]
}
```
Fit function returned best parameters for current train dataset:
```python
criterion='entropy', max_features=None, splitter='random'
```
Next, I build model with above parameters and fit model to train data (70% of whole data)
and test it on test data. Below graph shows confusion matrix for decision tree, how you can
see it's more accurate than EM clustering and has the same stability.

 ![Confusion matrix - decision tree](./model_fitting/plots/decision_tree_confusion_matrix.png)

#### Random forest
Set of decision trees, should be used when decision trees don't provide sufficient results. Find out more [here](https://towardsdatascience.com/random-forest-3a55c3aca46d).

##### Results
On the grounds of the decision tree I decided not to fit random forest model. Decision tree
provided well and accurate results so there is no reason to fit new model from the same family.

### Conclusions
Based on provided results I decided to use decision tree because it's the most accurate model.

## Web platform
### Description
I build web platform using:
- django,
- celery,
- redis.
Platform is retraining decision tree model every 3h and save it to SQLite database. Using REST API you can use last trained model to predict type of cancer. To do it you have to visit
http://127.0.0.1:8000/api/predict-cancer-type/radius/texture/perimeter/area/smoothness/compactness/concavity/concave_points/symmetry/fractal_dimension, where radius and etc are values provided by yourself.

### Run platfrom
1. Install [redis](https://tecadmin.net/install-redis-ubuntu/).
2. Install [python](https://realpython.com/installing-python/).
3. Create virtualenv
```python
python3 -m virtualenv env
```
4. Activate virtualenv
```python
source env/bin/activate
```
5. Install requirements
```python
pip install -r requirements.txt
```
6. Migrate
```python
python manage.py migrate
```

7. Run django server
```python
python manage.py runsever
```

8. Run celery tasks
```python
celery -A backend worker -l info -B
```

9. You are able to access http://127.0.0.1:8000/api/predict-cancer-type/1/1/1/1/1/1/1/1/1/1 .
