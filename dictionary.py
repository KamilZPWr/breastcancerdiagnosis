B = 'B'
M = 'M'
ID = 'id'
DIAGNOSIS = 'diagnosis'
MALIGNANT_CANCER = 'malignant_cancer'
MEAN = 'mean'
SE = 'se'
WORST = 'worst'
COLUMNS = [
    "radius_{}", "texture_{}",
    "perimeter_{}", "area_{}",
    "smoothness_{}", "compactness_{}",
    "concavity_{}", "concave points_{}",
    "symmetry_{}", "fractal_dimension_{}"
]
MEAN_COLUMNS = [column.format(MEAN) for column in COLUMNS]
SE_COLUMNS = [column.format(SE) for column in COLUMNS]
WORST_COLUMNS = [column.format(WORST) for column in COLUMNS]
FEATURES = {
    'Mean': MEAN_COLUMNS,
    'Standard error': SE_COLUMNS,
    '"Worst"': WORST_COLUMNS
}
