import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

from dictionary import MALIGNANT_CANCER, DIAGNOSIS, FEATURES, ID, M

data = pd.read_csv("../data/primary_data.csv", index_col=ID)
data[MALIGNANT_CANCER] = data.apply(lambda row: 1 if row.diagnosis == M else 0, axis=1)
data = data.drop(columns=[DIAGNOSIS])
data.to_csv('../data/transformed_data.csv', sep=';')
print('Number of nulls: \n', data.isnull().sum())

for features_group, columns in FEATURES.items():

    corr = data[columns].corr()

    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    f = plt.figure(figsize=(11, 9), dpi=100)

    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    sns.heatmap(corr, mask=mask, cmap="YlGnBu", center=0, vmax=1, vmin=-1,
                square=True, linewidths=.5, cbar_kws={"shrink": .5}, annot=True)
    f.suptitle(f'{features_group} features correlation', fontsize=20)
    plt.show()

cancer_type = data[MALIGNANT_CANCER]
print('Maligant cancer count:', cancer_type[cancer_type == 1].count(),
      'Bening cancer type:', cancer_type[cancer_type == 0].count())
