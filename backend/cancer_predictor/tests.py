from unittest.mock import Mock
from django.test import TestCase
from backend.cancer_predictor import views
from backend.cancer_predictor.views import NON_MODEL_NOW, MALIGNANT_CANCER, BENIGN_CANCER


class PredictionsTestCase(TestCase):
    def test_non_estimator(self):
        views.get_estimator = Mock(return_value=None)
        response = self.client.get('/api/predict-cancer-type/1/1/1/1/1/1/1/1/1/1')
        response_content = response.content.decode('ascii')
        assert response_content == NON_MODEL_NOW

    def test_malignant_cancer(self):
        views.get_estimator = Mock(return_value=True)
        views.predict = Mock(return_value=True)
        response = self.client.get('/api/predict-cancer-type/1/1/1/1/1/1/1/1/1/1')
        response_content = response.content.decode('ascii')
        assert response_content == MALIGNANT_CANCER

    def test_bening_cancer(self):
        views.get_estimator = Mock(return_value=True)
        views.predict = Mock(return_value=False)
        response = self.client.get('/api/predict-cancer-type/1/1/1/1/1/1/1/1/1/1')
        response_content = response.content.decode('ascii')
        assert response_content == BENIGN_CANCER
