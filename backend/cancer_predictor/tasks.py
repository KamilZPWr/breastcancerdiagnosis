from __future__ import absolute_import, unicode_literals
import numpy as np
import pandas as pd
from celery import task
from estimators.models import Estimator
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

from backend.cancer_predictor.models import BreastMeasures
from dictionary import MALIGNANT_CANCER

MIN_ACCURACY = 0.5
DESCRIPTION = 'Decision Tree Classifier {} accurate'


def get_weighted_accuracy(y_true, y_pred):
    conf_matrix = confusion_matrix(y_true, y_pred)
    conf_matrix_norm = conf_matrix.astype('float') / conf_matrix.sum(axis=1)[:, np.newaxis]

    sample_len = len(y_true)

    malignant_weight = y_true[y_true == 1].count()/sample_len
    benign_weight = y_true[y_true == 0].count()/sample_len

    malignant_score = conf_matrix_norm[0, 0]
    benign_score = conf_matrix_norm[1, 1]

    return malignant_score*malignant_weight + benign_score*benign_weight


def save_model(model, accuracy):
    old_estimator = Estimator.objects.last()
    if old_estimator:
        old_estimator.delete()

    estimator = Estimator(estimator=model)
    estimator.description = DESCRIPTION.format(accuracy)
    estimator.save()


def get_data():
    data = BreastMeasures.objects.all().filter(malignant_cancer__isnull=False)
    data = pd.DataFrame(data.values())
    data = data.drop(columns=['load_date', 'id'])
    return data.drop(columns=[MALIGNANT_CANCER]), data[MALIGNANT_CANCER]


@task
def train_decision_tree_model():
    X, y = get_data()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

    decision_tree_clf = DecisionTreeClassifier(criterion='entropy', max_features=None, splitter='random')
    decision_tree_clf.fit(X_train, y_train)

    y_pred = decision_tree_clf.predict(X_test)
    accuracy = get_weighted_accuracy(y_test, y_pred)

    if accuracy >= MIN_ACCURACY:
        save_model(decision_tree_clf, accuracy)
