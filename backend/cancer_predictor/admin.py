from django.contrib import admin
from backend.cancer_predictor.models import BreastMeasures, MachineLearningModel

admin.site.register(BreastMeasures)
admin.site.register(MachineLearningModel)
