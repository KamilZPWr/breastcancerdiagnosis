import pandas as pd
from django.http import HttpResponse
from estimators.models import Estimator

BENIGN_CANCER = 'Benign cancer'

MALIGNANT_CANCER = 'Malignant cancer'

NON_MODEL_NOW = "Sorry we don't have good model now."


def get_estimator():
    return Estimator.objects.last()


def predict(estimator, data):
    return estimator.estimator.predict(data)


def predict_cancer_type(request, radius, texture, perimeter, area, smoothness, compactness, concavity, concave_points, symmetry, fractal_dimension):
    data_row = {
        'radius': [float(radius)],
        'texture': [float(texture)],
        'perimeter': [float(perimeter)],
        'area': [float(area)],
        'smoothness': [float(smoothness)],
        'compactness': [float(compactness)],
        'concavity': [float(concavity)],
        'concave_points': [float(concave_points)],
        'symmetry': [float(symmetry)],
        'fractal_dimension': [float(fractal_dimension)]
    }

    data_row = pd.DataFrame(data_row)
    estimator = get_estimator()

    if estimator:
        result = predict(estimator, data_row)

        if result:
            return HttpResponse(MALIGNANT_CANCER)

        return HttpResponse(BENIGN_CANCER)

    return HttpResponse(NON_MODEL_NOW)

