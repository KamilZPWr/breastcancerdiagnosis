from django.apps import AppConfig


class CancerPredictorConfig(AppConfig):
    name = 'cancer_predictor'
