from rest_framework import serializers

from .models import BreastMeasures


class BreastMeasuresSerializer(serializers.ModelSerializer):
    class Meta:
        model = BreastMeasures
        fields = '__all__'
