from django.db import models


class BreastMeasures(models.Model):
    radius = models.FloatField(blank=False, null=False)
    texture = models.FloatField(blank=False, null=False)
    perimeter = models.FloatField(blank=False, null=False)
    area = models.FloatField(blank=False, null=False)
    smoothness = models.FloatField(blank=False, null=False)
    compactness = models.FloatField(blank=False, null=False)
    concavity = models.FloatField(blank=False, null=False)
    concave_points = models.FloatField(blank=False, null=False)
    symmetry = models.FloatField(blank=False, null=False)
    fractal_dimension = models.FloatField(blank=False, null=False)
    malignant_cancer = models.BooleanField(blank=False, null=True)
    load_date = models.DateField(auto_now_add=True)


class MachineLearningModel(models.Model):
    name = models.CharField(max_length=20, blank=False, null=False, primary_key=True)
    models_file = models.FileField(blank=False, null=False)
    load_date = models.DateField(auto_now_add=True)
