from __future__ import unicode_literals
import pandas as pd
from django.db import migrations

from backend.cancer_predictor.models import BreastMeasures

COLUMNS = [
    "radius", "texture",
    "perimeter", "area",
    "smoothness", "compactness",
    "concavity", "concave_points",
    "symmetry", "fractal_dimension",
    'malignant_cancer'
]


class Migration(migrations.Migration):

    def load_data(apps, schema_editor):
        data = pd.read_csv('./data/transformed_data.csv', sep=';')
        data = data[COLUMNS]

        BreastMeasures.objects.bulk_create(
            BreastMeasures(**vals) for vals in data.to_dict('records')
        )

    dependencies = [
        ('cancer_predictor', '0001_initial'),
    ]

    operations = [
            migrations.RunPython(load_data),
    ]
