# Generated by Django 2.2 on 2019-04-05 21:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BreastMeasures',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('radius', models.FloatField()),
                ('texture', models.FloatField()),
                ('perimeter', models.FloatField()),
                ('area', models.FloatField()),
                ('smoothness', models.FloatField()),
                ('compactness', models.FloatField()),
                ('concavity', models.FloatField()),
                ('concave_points', models.FloatField()),
                ('symmetry', models.FloatField()),
                ('fractal_dimension', models.FloatField()),
                ('malignant_cancer', models.BooleanField(null=True)),
                ('load_date', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='MachineLearningModel',
            fields=[
                ('name', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('models_file', models.FileField(upload_to='')),
                ('load_date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
