from django.urls import path

from . import views

urlpatterns = [
    path(
        r'<str:radius>/'
        r'<str:texture>/'
        r'<str:perimeter>/'
        r'<str:area>/'
        r'<str:smoothness>/'
        r'<str:compactness>/'
        r'<str:concavity>/'
        r'<str:concave_points>/'
        r'<str:symmetry>/'
        r'<str:fractal_dimension>',
        views.predict_cancer_type, name='get'
    ),
]
