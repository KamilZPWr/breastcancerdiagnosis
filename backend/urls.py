from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/predict-cancer-type/', include('backend.cancer_predictor.urls'))
]
