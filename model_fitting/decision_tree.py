import pandas as pd
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
import numpy as np
from matplotlib import pyplot as plt
from dictionary import MEAN_COLUMNS, MALIGNANT_CANCER
from model_fitting.utils import plot_confusion_matrix

data = pd.read_csv('../data/transformed_data.csv', sep=';', index_col='id')
mean_data = data[MEAN_COLUMNS + [MALIGNANT_CANCER]]
mean_data_columns = mean_data.columns
min_max_scaler = MinMaxScaler()
mean_data = min_max_scaler.fit_transform(mean_data)
mean_data = pd.DataFrame(mean_data, columns=mean_data_columns)
mean_data[MALIGNANT_CANCER] = mean_data[MALIGNANT_CANCER].astype(int)

X_train, X_test, y_train, y_test = train_test_split(mean_data[MEAN_COLUMNS], mean_data[MALIGNANT_CANCER], test_size=0.3, random_state=0)

decision_tree_clf = DecisionTreeClassifier()

params = {
    'criterion': ['entropy', 'gini'],
    'splitter': ['best', 'random'],
    'max_features': ['sqrt', 'log2', None]
}

grid_search = GridSearchCV(estimator=decision_tree_clf, param_grid=params, cv=5, iid=True)
grid_search.fit(X_train, y_train)
print(grid_search.best_score_, grid_search.best_params_)

labels = ('M', 'B')

decision_tree_clf = DecisionTreeClassifier(criterion='entropy', max_features=None, splitter='random')
decision_tree_clf.fit(X_train, y_train)
y_pred = decision_tree_clf.predict(X_test)

tmp = np.array(labels)
plot_confusion_matrix(y_test, y_pred, classes=tmp, normalize=True,
                      title='Normalized confusion matrix')

plt.show()
