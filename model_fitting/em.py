import pandas as pd
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot as plt
import numpy as np
from dictionary import MEAN_COLUMNS, MALIGNANT_CANCER
from model_fitting.utils import scatter_plot, plot_confusion_matrix

data = pd.read_csv('../data/transformed_data.csv', sep=';', index_col='id')
mean_data = data[MEAN_COLUMNS + [MALIGNANT_CANCER]]
mean_data_columns = mean_data.columns
min_max_scaler = MinMaxScaler()
mean_data = min_max_scaler.fit_transform(mean_data)
mean_data = pd.DataFrame(mean_data, columns=mean_data_columns)
mean_data[MALIGNANT_CANCER] = mean_data[MALIGNANT_CANCER].astype(int)
pca = PCA()
pca.fit(mean_data[MEAN_COLUMNS])

plt.bar(range(1, 11), pca.explained_variance_ratio_, alpha=0.5, align='center', label='Explained variance')
plt.step(range(1, 11), np.cumsum(pca.explained_variance_ratio_), where='mid', label='Cumulative explained variance')
plt.ylabel('Coefficient of explained variance')
plt.xlabel('Components')
plt.title("PCA Analysis")
plt.legend(loc='best')
plt.show()

pca = PCA(n_components=2)
X = pca.fit_transform(mean_data[MEAN_COLUMNS])
y = mean_data[MALIGNANT_CANCER]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)


gaussian_mixture_clf = GaussianMixture(n_components=2)
gaussian_mixture_clf.fit(X_train, y_train)
y_pred = gaussian_mixture_clf.predict(X_test)

labels = ('M', 'B')
scatter_plot(X_test, y_test, labels, 'True values')
scatter_plot(X_test, y_pred, labels, 'Predicted values')

tmp = np.array(labels)
plot_confusion_matrix(y_test, y_pred, classes=tmp, normalize=True,
                      title='Normalized confusion matrix')
plt.show()
